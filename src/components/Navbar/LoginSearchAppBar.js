import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import Login from '../Login';
import { Link } from 'react-router-dom';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import HomeIcon from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export function LoginSearchAppBar() {
  
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);


  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ background: '#C4C4C4'}}>
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="space-between"
          >
            <Grid
              item
              component={Link} to={{pathname: '/'}}
            >
              <IconButton>
                <HomeIcon/>
              </IconButton>
            </Grid>
            <Grid
              item
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <PopupState variant="popover" popupId="demo-popup-menu">
                {popup => (
                  <React.Fragment>
                    <Button 
                      style={{margin: 5, background: "#A6A5F1"}}
                      variant="contained" {...bindTrigger(popup)}>
                      Sign In
                    </Button>
                    <Menu {...bindMenu(popup)}>
                      <Login/>
                    </Menu>
                  </React.Fragment>
                )}
              </PopupState>
              <Button 
                style={{margin: 5, background: "#F14646"}} 
                component={Link} to={{pathname: '/signup'}}
              >
                SignUp
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}
