import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import Grid from '@material-ui/core/Grid';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import MoreIcon from '@material-ui/icons/MoreVert';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import MenuIcon from '@material-ui/icons/Menu'
import { postLogoutUser } from '../UserFunctions'
import { Link } from 'react-router-dom'
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export function LoggedSearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleHamburgerMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const menuIdcategory = 'primary-search-category-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose} component={ Link } to={{pathname: '/profile'}}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose} component={ Link } to={{pathname: '/myproject/'}} style={{padding: '5px '}}>My Project</MenuItem>
      <MenuItem onClick={postLogoutUser}>Logout</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
      <MenuItem>
        <iconButton 
          aria-label="show category"
          aria-controls="primary-search-category-menu"
          aria-haspopup="true"
          color="inherit"
        >
            <MenuIcon />
        </iconButton>
        <p>Category</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ background: '#C4C4C4'}}>
        <Toolbar>
          <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="flex-start"
          >
            <PopupState variant="popover" popupId="demo-popup-menu">
              {popup => (
                <React.Fragment>
                  <IconButton 
                    style={{margin: 5, background: "#C4C4C4"}}
                    variant="outlined" {...bindTrigger(popup)}
                    component={ Link } to={{pathname: '/myproject'}}
                  >
                    <BusinessCenterIcon/>
                  </IconButton>
                </React.Fragment>
              )}
            </PopupState>
            
            <PopupState variant="popover" popupId="demo-popup-menu">
              {popup => (
                <React.Fragment>
                  <IconButton 
                    style={{margin: 5, background: "#C4C4C4"}}
                    variant="outlined" {...bindTrigger(popup)}>
                    <AccountCircle/>
                  </IconButton>
                  <Menu {...bindMenu(popup)}>
                    <MenuItem onClick={handleHamburgerMenuOpen} component={ Link } to={{pathname: '/myprofile'}}>Profile</MenuItem>
                    <MenuItem onClick={postLogoutUser}>Logout</MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
            <PopupState variant="popover" popupId="demo-popup-menu">
              {popup => (
                <React.Fragment>
                  <IconButton 
                    style={{margin: 5, background: "#C4C4C4"}}
                    variant="outlined"
                    component={ Link } to={{pathname: '/'}}
                  >
                    <HomeIcon/>
                  </IconButton>
                </React.Fragment>
              )}
            </PopupState>
            
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}
