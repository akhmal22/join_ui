import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
      maxWidth: 345,
  },
}));

// GET request method in ./Get
export const Projects = ({ projects }) => {
  const classes = useStyles();

  return (
    <div style={{backgroundColor: 'white'}}>
      <h3 style={{padding: '20px', margin: '5px', paddingLeft: '25px'}}> Top Project </h3>
      <Grid container spacing={3} style={{padding: '15px', margin: '5px'}}>
        {projects.map((project) => (
          <Grid item xs={3}>
              <Card className={classes.card} component={Link} to={{pathname:'/project/'+project.id}}>
                  <CardActionArea>
                      <CardMedia
                      className={classes.media}
                      style={{ height: 0, paddingTop: '90%'}}
                      image={require("./Bibliothèque_de_l'Assemblée_Nationale_(Lunon).jpg")}
                      title={project.name}
                      />
                      <CardContent style={{padding: 'center'}}>
                      <Typography gutterBottom variant="body" fontSize="14px" fontWeight="bold" color="black">
                          {project.name}
                      </Typography>
                      <Typography variant="body2" color="textSecondary" fontSize="14px" fontWeight="regular">
                          {project.description}
                      </Typography>
                      </CardContent>
                  </CardActionArea>
              </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  )
};