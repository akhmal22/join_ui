import React, { Component } from 'react';
import { Grid, Avatar, Button, Typography } from '@material-ui/core';
import MergeTypeIcon from '@material-ui/icons/MergeType';
import LinkIcon from '@material-ui/icons/Link';
import {postRegisterCollab} from '../../UserFunctions'

class Details extends Component{
    constructor(props){
        super(props);
        this.state = {
            id: null,
            name: null,
            description: null, 
            type: null,
            due_date: null, 
            num_req_collaborator: null,
            owner: []
            
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        const {id} = this.props.match.params;
        fetch('http://35.223.215.2:5000/project/'+id)
        .then(res => res.json())
        .then((data) => {
            this.setState({name: data.name})
            this.setState({description: data.description})
            this.setState({type: data.type})
            this.setState({due_date: data.due_date})
            this.setState({num_req_collaborator: data.num_req_collaborator})
            this.setState({owner: data.owner})
            this.setState({id: data.id})
        })
        .catch(console.log)
    }

    handleSubmit(event){
        event.preventDefault();
        postRegisterCollab(this.state.id);
    }

    render(){
        console.log(this.state.owner.id)
        if(localStorage.getItem('user_id')===null){
            return(
                <h3 style={{textAlign: "center"}}>Please Login (ERR401)</h3>
            )
        }else if(this.state.owner.id!==parseInt(localStorage.getItem('user_id'))){
            return(
                <div className="container-full" >
                    <Typography 
                        variant="h3" 
                        align="center"
                        style={{ color: '#000000'}}
                    >
                        <strong>PROJECT DETAIL</strong>
                    </Typography>
                    <Grid container direction="row" style={{padding: '1%'}}>
                        <Grid item>
                        <Avatar style={{height: '35vh', width: '35vh'}}><h1>P</h1></Avatar>
                        </Grid>
                        <Grid item style={{margin: "15px"}}>
                        <Typography
                                variant="h4" 
                                style={{ color: '#000000'}}
                            >
                                {this.state.name}
                            </Typography>
                            <Typography
                                variant="h4"
                                style={{color: '#000000'}}
                            >
                                <strong>{this.state.type}</strong>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="flex-start"
                    >
                        <Grid style={{margin: '5%'}}>
                            <Button 
                                style={{margin: '5px', background: '#6D6767'}}
                                variant="contained"
                                onClick={this.handleSubmit}
                            >
                                <LinkIcon/>
                                Join
                            </Button>
                            <br/><br/><br/>
                            <Typography variant='h4'>
                                <strong>Due Date</strong>
                                <p>{this.state.due_date}</p>
                            </Typography>
                            <br/>
                            <Typography variant='h4'>
                                <strong>Description</strong>
                                <p>{this.state.description}</p>
                            </Typography>
                            <br/>
                        </Grid>
                    </Grid>
                </div>
            )
        }else{
            return(
                <div className="container-full" >
                    <Typography 
                        variant="h3" 
                        align="center"
                        style={{ color: '#000000'}}
                    >
                        <strong>PROJECT DETAIL</strong>
                    </Typography>
                    <Grid container direction="row" style={{padding: '1%'}}>
                        <Grid item>
                        <Avatar style={{height: '35vh', width: '35vh'}}><h1>P</h1></Avatar>
                        </Grid>
                        <Grid item style={{margin: "15px"}}>
                        <Typography
                                variant="h4" 
                                style={{ color: '#000000'}}
                            >
                                {this.state.name}
                            </Typography>
                            <Typography
                                variant="h4"
                                style={{color: '#000000'}}
                            >
                                <strong>{this.state.type}</strong>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="flex-start"
                    >
                        <Grid style={{margin: '5%'}}>
                            <Button 
                                style={{margin: '5px', background: '#6D6767'}}
                                variant="disabled"
                                onClick={this.handleSubmit}
                            >
                                <LinkIcon/>
                                Join
                            </Button>
                            <br/><br/><br/>
                            <Typography variant='h4'>
                                <strong>Due Date</strong>
                                <p>{this.state.due_date}</p>
                            </Typography>
                            <br/>
                            <Typography variant='h4'>
                                <strong>Description</strong>
                                <p>{this.state.description}</p>
                            </Typography>
                            <br/>
                        </Grid>
                    </Grid>
                </div>
            )
        }
        
    }
}

export default Details