import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import { Button } from '@material-ui/core';
import { postRegister } from './CRUDFoo';
import { Link } from 'react-router-dom';

class Addproject extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            desc: '',
            type: '',
            due_date: '',
            req_collab: 1,
            redirect: false
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange() {
        let path = '/myproject';
        this.props.history.push(path);
    }

    handleChange(event){
        this.setState({ [event.target.name]: event.target.value})
    }

    handleSubmit(event){
        event.preventDefault();
        if(localStorage.getItem('user_id')===null){
            window.location.reload();
        }else{
            postRegister(this.state.name,this.state.desc,this.state.type,
                this.state.due_date,
                this.state.req_collab);
        }
        this.routeChange()
    }

    render () {
        return (
            <Form style={{margin: '5%'}}>
                <h3 className="text-center" style={{paddingBottom: '10px', fontWeight: 'bold'}}>Add Project</h3>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Project Name</Form.Label>
                    <Form.Control name="name" onChange={this.handleChange} type="text" placeholder="" required/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Project Description</Form.Label>
                    <Form.Control name="desc" onChange={this.handleChange} type="text" as="textarea" row="3" placeholder="Text here..." required/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Project Type</Form.Label>
                    <Form.Control name="type" onChange={this.handleChange} type="text" placeholder="" required/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Collaborator (max: 20)</Form.Label>
                    <Form.Control name="req_collab" onChange={this.handleChange} type="number" placeholder="" min="1" max="20"required/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Form.Label>Project Due Date</Form.Label>
                    <Form.Control name="due_date" onChange={this.handleChange} type="text" placeholder="yyyy-mm-dd" required/>
                </Form.Group>
                <div style={{justifyContent: 'center'}}>
                <Button
                style={{margin: 5, background: "#E7E6E6",}}
                component={Link} to={{pathname: "/myproject"}}
                >
                Cancel
                </Button>
                <Button 
                style={{margin: 5, background: "#EF5D5D"}}
                onClick={this.handleSubmit}
                >
                Finish
                </Button>
                </div>
            </Form>
        )
    }
}

export default Addproject
