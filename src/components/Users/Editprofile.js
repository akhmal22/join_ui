import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import { Button, ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { putEditUser } from '../UserFunctions';

class Editprofile extends Component {
    constructor(props){
        super(props);
        this.state = {
            fname: null,
            phone: null,
            addr: null,
            org: null,
            pos: null
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        this.setState({ [event.target.name]: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        putEditUser(this.state.fname,this.state.phone,
                    this.state.addr,this.state.org,this.state.pos)
        alert("edit success")
    }

    render() {

        return (
            <Form style={{marginTop: '5%',marginBottom: '15%', marginLeft: '15%', marginRight: '15%'}}>
                <h3 style={{marginLeft: '40%', paddingBottom: '20px', fontWeight: 'bold'}}>Edit Profile</h3>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control required name="fname" type="text" placeholder="" onChange={this.handleChange} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control required name="phone" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Address</Form.Label>
                    <Form.Control required name="addr" type="text" as="textarea" rows="3" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Organization</Form.Label>
                    <Form.Control required name="org" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Position</Form.Label>
                    <Form.Control required name="pos" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Button
                style={{margin: 5, background: "#E7E6E6",}}>
                Cancel
                </Button>
                <Button 
                style={{margin: 5, background: "#EF5D5D"}}
                onClick={this.handleSubmit}
                >
                Finish
                </Button>
            </Form>
        )
    }
}

export default Editprofile