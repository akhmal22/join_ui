import React, { Component } from 'react'
import { postRegisterLicense } from '../../UserFunctions';
import Form from 'react-bootstrap/Form'
import {Button} from '@material-ui/core'

class License extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            org: '',
            start: '',
            end: '',
            desc: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        this.setState({ [event.target.name]: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        postRegisterLicense(this.state.name,this.state.org,this.state.start,this.state.end,this.state.desc);
        alert('license added')
        window.location.reload()
    }

    render() {
        return (
            <Form>
                <h4>Add License</h4>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>License Name</Form.Label>
                    <Form.Control name="name" required type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Organization</Form.Label>
                    <Form.Control name="org" required type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control name="start" required type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control name="end" required type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Description</Form.Label>
                    <Form.Control name="desc" required type="text" as="textarea" row="3" placeholder="" onChange={this.handleChange} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Type "CONFIRM"</Form.Label>
                    <Form.Control required type="text" placeholder="" />
                </Form.Group>
                <Button
                style={{margin: 5, background: "#E7E6E6",}}>
                Cancel
                </Button>
                <Button 
                style={{margin: 5, background: "#EF5D5D"}}
                onClick={this.handleSubmit}
                >
                Finish
                </Button>
            </Form>
        )
    }
}

export default License