import React, { Component } from 'react'
import { postRegisterExperience } from '../../UserFunctions';
import Form from 'react-bootstrap/Form'
import {Button} from '@material-ui/core'

class Experience extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            company: '',
            start_date: '',
            end_date: '',
            description: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        this.setState({ [event.target.name]: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        postRegisterExperience(this.state.name,this.state.company,this.state.start_date,this.state.end_date,this.state.description);
        alert("experience added")
        window.location.reload()
    }

    render() {
        return (
            <Form>
                <h4>Add Experience</h4>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Experience Name</Form.Label>
                    <Form.Control required name="name" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Company</Form.Label>
                    <Form.Control required name="company" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control required name="start_date" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control required name="end_date" type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Description</Form.Label>
                    <Form.Control required name="description" type="text" as="textarea" row="3" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>type "CONFIRM"</Form.Label>
                    <Form.Control required type="text" placeholder="" onChange={this.handleChange}/>
                </Form.Group>
                <Button
                style={{margin: 5, background: "#E7E6E6",}}>
                Cancel
                </Button>
                <Button 
                style={{margin: 5, background: "#EF5D5D"}}
                onClick={this.handleSubmit}
                >
                Finish
                </Button>
            </Form>
        )
    }
}

export default Experience