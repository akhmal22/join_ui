import React, { Component } from 'react'
import {
    Paper,
    Typography,
    Avatar,
    Grid,
    Button,
    ExpansionPanel
} from '@material-ui/core'
import {Link} from 'react-router-dom';
import GetSkills from './Profiles/GetSkills';
import GetExperiences from './Profiles/GetExperiences';
import GetLicences from './Profiles/GetLicences';
import GetPortfolios from './Profiles/GetPortfolios';

class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            users: [],
            date_created: null
        };
    
    }

    componentDidMount(){
        const id = localStorage.getItem('user_id')
        fetch('http://35.223.215.2:5000/user/'+id)
        .then(res => res.json())
        .then((data) => {
            this.setState({users: data});
            this.setState({date_created: data.date_created.split(',')[0]})
        })
        .catch(console.log)
    }

    render(){
        if(localStorage.getItem('user_id')===null){
            return (
                <h3 style={{textAlign: 'center'}}>Please Login (ERR401)</h3>
            )
        }else{
            return (
                <div className="container-full" >
                    <Paper
                        className="bg-dark"
                        component="div"
                        
                    >
                        <Typography 
                        variant="h2" 
                        align="center"
                        style={{ color: '#FFFFFF'}}
                        >
                        MY PROFILE
                        </Typography>
                        <Grid container direction="row" alignItems="center" style={{padding: '1%'}}>
                            <Grid item>
                                <Avatar alt="Remy Sharp" src={require("./boy.png")} style={{height: "200px", width: "200px"}} />
                            </Grid>
                            <Grid item xs={9} container direction="column" spacing={2}>
                                <Typography style={{color:'#4DE076', fontSize: '30px', fontWeight: 'bold'}}>
                                    <p>{this.state.users.username}</p>
                                </Typography>
                                <Typography style={{color:'#FFFFFF', fontSize: '30px'}}>
                                    <strong>{this.state.users.position}</strong><br/>
                                    {this.state.users.organization}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="flex-start"
                    >
                        <Grid style={{margin: '5%'}}>
                            <Button variant="contained"
                                component={ Link } to={{pathname: '/profile/edit'}}
                            >
                                Edit Profile
                            </Button>
                            <br/><br/><br/>
                            <Typography variant='h4'>
                                <strong>Experience</strong>
                            </Typography>
                            <GetExperiences/>
                            <br/>
                            <Typography variant='h4'>
                                <strong>Skills</strong>
                            </Typography>
                            <GetSkills/>
                            <br/>
                            <Typography variant='h4'>
                                <strong>Licenses</strong>
                            </Typography>
                            <GetLicences/>
                            <br/>
                        </Grid>
                        <Grid style={{margin: '5%'}}>
                        <Button style={{margin: '5px'}} variant="contained"
                        component={ Link } to={{pathname: '/myproject'}}>My Project</Button>
                        
                        <br/><br/><br/>
                        <Typography variant='h4'>
                            <strong>Has been member since</strong>
                            <p>{this.state.date_created}</p>
                        </Typography>
                        <br/>
                        <Typography variant='h4'>
                            <strong>Project involved</strong>
                            <p>{this.state.users.num_projects}</p>
                        </Typography>
                        <br/>
                        <Typography variant='h4'>
                            <strong>Portfolios</strong>
                        </Typography>
                        <GetPortfolios/>
                        <br/>
                        </Grid>
                    </Grid>
                </div>
            )
        }
    }
}

export default Profile
