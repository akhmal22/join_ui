import React, { Component } from 'react';
import MyProject from './Projects/MyProjects';
import Button from '@material-ui/core/Button';
import { Grid, Paper} from '@material-ui/core/';
import {Link} from 'react-router-dom';

class Myproject extends Component {
    
    render() {
        if(localStorage.getItem('user_id')===null){
            return (
                <h3 style={{paddingTop: 20, textAlign: 'center'}}>Please Login (ERR401)</h3>
            )
        }else{
            return (
                <div style={{backgroundColor: 'white'}}>
                    <Grid style={{margin: '10px', marginBottom: '5px'}}
                        justify="flex-end"
                    >
                        <Paper>
                            <h3 style={{
                                position: "absolute",
                                width: "231px",
                                height: "56px",
                                left: "49px",
                                top: "292px",
                                fontFamily: "Roboto",
                                fontStyle: "normal",
                                fontWeight: 500,
                                fontSize: "36px",
                                lineHeight: "56px",
                                textAlign: "center"}}
                            >
                                My Project</h3>
                            <Button 
                                style={{
                                    position: "absolute",
                                    width: "135px",
                                    height: "50px",
                                    left: "80px",
                                    top: "370px",
                                    background: "#878787"}}
                                    variant="contained"
                                    component={Link} to={{pathname: '/myproject/add'}}
                                >
                                Add project
                            </Button>
                        </Paper>
                    </Grid>
                    <div><MyProject/></div>
                </div>
            )
        }
    }
        
}

export default Myproject