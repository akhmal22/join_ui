import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Paper,
    Typography,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary 
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Skills from '../AddProfile/Skill';


const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    card: {
        maxWidth: 345,
    },
}));

class GetExperience extends Component {
  constructor(props){
    super(props);
    this.state = {
        skill: []
    };

  }

  componentDidMount(){
    const id = localStorage.getItem('user_id')
    fetch('http://35.223.215.2:5000/user/'+id)
    .then(res => res.json())
    .then((data) => {
        this.setState({skill: data.profiles.skills});
    })
    .catch(console.log)
  }

  render(){
    if(this.state.skill.length!==0){
      return (
        <div>
          {this.state.skill.map((skill) => (
              <Paper>
                  <Typography gutterBottom variant="h5" component="p">
                      {skill.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="h5">
                      {skill.familiarity}
                  </Typography>
              </Paper>
          ))}
          <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                Even more Skills? Add it now!
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Skills/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>                                                                                                                                                                                                                                                                                                                                                                                                                                        
      )
    }else{
      return(
        <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                No Skills, add it instead?
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Skills/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
      )
    }
  }
}

export default GetExperience;