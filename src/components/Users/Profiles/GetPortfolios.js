import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Paper,
    Typography,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary 
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Portfolio from '../AddProfile/Portfolio';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    card: {
        maxWidth: 345,
    },
}));

class GetExperience extends Component {
  constructor(props){
    super(props);
    this.state = {
        portfolio: []
    };

  }

  componentDidMount(){
    const id = localStorage.getItem('user_id')
    fetch('http://35.223.215.2:5000/user/'+id)
    .then(res => res.json())
    .then((data) => {
        this.setState({portfolio: data.profiles.portfolios});
    })
    .catch(console.log)
  }

  render(){
    if(this.state.portfolio.length!==0){
      return (
        <div>
          {this.state.portfolio.map((portfolio) => (
              <Paper>
                  <Typography gutterBottom variant="h5" component="p">
                      {portfolio.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                      {portfolio.url}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                      Start: {portfolio.start_date}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="h2">
                      End: {portfolio.end_date}
                  </Typography>
                  <Typography gutterBottom variant="h5" component="h2">
                      {portfolio.description}
                  </Typography>
              </Paper>
          ))}
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                >
                    Even more Portfolio? add it now!
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Portfolio/>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
      )
    }else{
      return(
        <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                No Portfolio, add it instead?
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Portfolio/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
      )
    }
  }
}

export default GetExperience;