import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Paper,
    Typography,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary 
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import License from '../AddProfile/License';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    card: {
        maxWidth: 345,
    },
}));

class GetExperience extends Component {
  constructor(props){
    super(props);
    this.state = {
        licence: []
    };

  }

  componentDidMount(){
    const id = localStorage.getItem('user_id')
    fetch('http://35.223.215.2:5000/user/'+id)
    .then(res => res.json())
    .then((data) => {
        this.setState({licence: data.profiles.licences});
    })
    .catch(console.log)
  }

  render(){
    if(this.state.licence.length!==0){
      return (
        <div>
          {this.state.licence.map((licence) => (
              <Paper>
                  <Typography gutterBottom variant="h5" component="p">
                      {licence.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                      {licence.organization}
                  </Typography>
                  <Typography gutterBottom variant="h5" component="h2">
                      {licence.description}
                  </Typography>
                  <Typography gutterBottom variant="h5" component="h2">
                      {licence.credential}
                  </Typography>
              </Paper>
          ))}
          <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                Even more License? Add it now!
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <License/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
        </div>
      )
    }else{
      return(
        <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                No License, add it instead?
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <License/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
      )
    }
  }
}

export default GetExperience;