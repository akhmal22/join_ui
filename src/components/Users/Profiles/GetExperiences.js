import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Paper,
    Typography,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary 
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Experience from '../AddProfile/Experience';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    card: {
        maxWidth: 345,
    },
}));

class GetExperience extends Component {
  constructor(props){
    super(props);
    this.state = {
        exprience: []
    };

  }

  componentDidMount(){
    const id = localStorage.getItem('user_id')
    fetch('http://35.223.215.2:5000/user/'+id)
    .then(res => res.json())
    .then((data) => {
        this.setState({exprience: data.profiles.expriences});
    })
    .catch(console.log)
  }

  render(){
    if(this.state.exprience.length!==0){
      return (
        <div>
          {this.state.exprience.map((exprience) => (
              <Paper>
                  <Typography gutterBottom variant="h5" component="p">
                      {exprience.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                      {exprience.company}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                      Start: {exprience.start_date}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="h2">
                      End: {exprience.end_date}
                  </Typography>
                  <Typography gutterBottom variant="h5" component="h2">
                      {exprience.description}
                  </Typography>
              </Paper>
          ))}
          <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                Even more Experience? Add it now!
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Experience/>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      )
    }else{
      return(
        <ExpansionPanel>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
            >
                No Experience, add it instead?
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Experience/>
            </ExpansionPanelDetails>
        </ExpansionPanel>
      )
    }
  }
}

export default GetExperience;