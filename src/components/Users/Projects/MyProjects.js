import React, { Component } from 'react';
import {Projects} from './Projects';

class MyProject extends Component {
    state = {
        projects: []
    }

    componentDidMount(){
        fetch('http://35.223.215.2:5000/projects',{headers:{'Access-Control-Allow-Origin': '*'}})
        .then(res => res.json())
        .then((data) => {
            for(var props in data){
                if(data[props].owner.id!==parseInt(localStorage.getItem('user_id'))){
                    delete data[props]
                }
                this.setState({projects: data})
            }
        })
        .catch(console.log)
    }

    render () {
        return (
            <Projects projects={this.state.projects} />
        )
    }
}

export default MyProject