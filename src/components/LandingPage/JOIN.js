import React, { Component } from 'react';
import './Homepage.css';
import Searchfield from './Searchfield';

class Homepage1 extends Component {

  render() {
    return (
      <div className="container-full bg-dark">
        <div className="col-sm-8 mx-auto">
            <h1 className="text-center white-text" style={{paddingTop: '20px'}}>
                JOIN
            </h1>
            <h6 className="text-center white-text" style={{paddingBottom: '20px'}}>
                Wanna Join?
            </h6>
            <div style={{paddingBottom: '10px'}}>
            <Searchfield/>
            </div>
        </div>
      </div>
    )
  }

}

export default Homepage1