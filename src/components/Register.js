import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import { postRegisterUser } from './UserFunctions';
import { Button } from '@material-ui/core';

class Register extends Component {
  constructor(props){
    super(props);
    this.state = {
      fullname: null,
      username: null,
      phone: null,
      address: null,
      email: null,
      password: null,
      organization: null,
      position: null,
      confirmPassword: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.routeChange = this.routeChange.bind(this);
  }

  handleChange(event){
    this.setState({ [event.target.name]: event.target.value})
  }

  routeChange() {
    let path = '/';
    this.props.history.push(path);
  }

  handleSubmit(event){
    const { password, confirmPassword } = this.state;
    if(password !== confirmPassword){
      alert("Password don't match!");
    }else{
      postRegisterUser(this.state.fullname,
        this.state.username,
        this.state.phone,
        this.state.address,
        this.state.email,
        this.state.password,
        this.state.organization,
        this.state.position,);
    }
    event.preventDefault();
    this.routeChange()
  }

  render() {
    return (
      <Form style={{margin: '5%'}}>
        <h3 style={{textAlign: "center"}}>Sign Up</h3>
        <Form.Group controlId="exampleForm.ControlInput1">
            <Form.Label>Fullname</Form.Label>
            <Form.Control name="fullname" onChange={this.handleChange} type="text" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Username</Form.Label>
            <Form.Control name="username" onChange={this.handleChange} type="text" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control name="phone" onChange={this.handleChange} type="tel" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Address</Form.Label>
            <Form.Control name="address" onChange={this.handleChange} type="text" as="textarea" row="3" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Email</Form.Label>
            <Form.Control name="email" onChange={this.handleChange} type="email" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Organization</Form.Label>
            <Form.Control name="organization" onChange={this.handleChange} type="text" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Position</Form.Label>
            <Form.Control name="position" onChange={this.handleChange} type="text" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Password</Form.Label>
            <Form.Control name="password" onChange={this.handleChange} type="password" placeholder="" required/>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control name="confirmPassword" onChange={this.handleChange} type="password" placeholder="" required/>
        </Form.Group>
        <Button
        style={{margin: 5, background: "#E7E6E6",}}
        onClick={this.routeChange}
        >
          Cancel
        </Button>
        <Button 
        style={{margin: 5, background: "#EF5D5D"}}
        onClick={this.handleSubmit}
        >
          Finish
        </Button>
      </Form>
    )
  }
}

export default Register;