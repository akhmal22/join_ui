import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact'
import { postLoginUser} from './UserFunctions'
import { Button } from '@material-ui/core';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
        username:'',
        password: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event){
    this.setState({ [event.target.name]: event.target.value})
  }

  handleSubmit(event){
    event.preventDefault();
    this.state.token = postLoginUser(this.state.username, this.state.password);
    console.log("Bearer " + localStorage.getItem('access_token'))
    setTimeout(() => window.location.reload(), 2500);
  }

  render() {
      return (
        <MDBContainer >
          <MDBRow>
            <MDBCol className="center">
              <form>
                <p className="h4 text-center mb-4 md-form">Sign in</p>
                <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
                  Username
                </label>
                <input
                type="text"
                id="defaultFormRegisterNameEx"
                className="form-control"
                name="username"
                onChange={this.handleChange}
                />
                <br />
                <label
                  htmlFor="defaultFormRegisterPasswordEx"
                  className="grey-text"
                >
                  Password
                </label>
                <input
                  type="password"
                  id="defaultFormRegisterPasswordEx"
                  className="form-control"
                  name="password"
                  onChange={this.handleChange}
                />
                <br />
                <div className="text-center mt-4" >
                  <Button style={{backgroundColor: '#A6A5F1', margin: '10px'}} type="submit"
                  onClick={this.handleSubmit}
                  >
                    Login
                  </Button>
                </div>
              </form>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      )
    
  }
}

export default Login;